io.popen("cd ~/.config/mpv/discord_rpc && python3 discord_rpc_helper.py")

function write_vars()
    filename = mp.get_property("filename")
    metadata = mp.get_property_native("filtered-metadata")

    if metadata["Title"] == nil then --TODO smarter tag processing system
        text_string = filename .. "\n" .. "no album"
    elseif metadata["Album"] == nil then
        text_string = metadata["Artist"] .. " - " .. metadata["Title"] .. "\n" ..
        "Single"
    else
        text_string = metadata["Artist"] .. " - " .. metadata["Title"] .. "\n" .. 
        "on album " .. metadata["Album"]
    end

    io.open(os.getenv("HOME") .. "/.config/mpv/discord_rpc/discord_rpc_helper.txt", "w"):close() --Clear text file
    rpc = io.open(os.getenv("HOME") .. "/.config/mpv/discord_rpc/discord_rpc_helper.txt", "w+")
    rpc:write(text_string)
    rpc:close()
end

function cleanup()
    os.execute("pkill python3") --TODO proper kill of only the initial popen
end

mp.register_event("file-loaded", write_vars)

mp.register_event("shutdown", cleanup)